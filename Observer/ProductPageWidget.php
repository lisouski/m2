<?php

namespace Photoslurp\Pswidget\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\View\LayoutInterface;

class ProductPageWidget implements ObserverInterface
{
    protected $_storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
    }

    public function execute(Observer $observer)
    {
        if ($observer->getFullActionName() == 'catalog_product_view') {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $model = $objectManager->create('\Photoslurp\Pswidget\Model\Items');
            $collection = $model->getCollection();
            $collection->addFilter('page_type', 'product');
            if (count($this->_storeManager->getWebsites())>1) {
                $collection->addFilter('website', $this->_storeManager->getStore()->getWebsiteId());
            }
            $widgetId = $collection->getFirstItem()->getId();

            if ($widgetId) {
                $layout = $observer->getLayout();

                $block = $layout->createBlock(
                    'Photoslurp\Pswidget\Block\Widget',
                    'pswidget_product',
                    ['data' => ['widget_id' => $widgetId,]]
                );

                $params = $block->getParams();

                $layout->setChild('content', 'pswidget_product', 'pswidget_product');

                $layout->reorderChild('content', 'pswidget_product', $params['position'], $after = true);
            }
        }
    }
}
