<?php

namespace Photoslurp\Pswidget\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\View\LayoutInterface;

class Conversion implements ObserverInterface
{

    protected $_storeManager;

    protected $_scopeConfig;

    protected $_cookieManager;

    protected $_orderFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
    
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_cookieManager = $cookieManager;
        $this->_orderFactory = $orderFactory;
    }

    public function execute(Observer $observer)
    {
        if ($this->_scopeConfig->getValue('tracking_section/general/tracking_enable')) {
            $orderIds = $observer->getEvent()->getOrderIds();
            if (count($orderIds)) {
                $orderId = $orderIds[0];
                $order = $this->_orderFactory->create()->load($orderId);
                $items = $order->getAllVisibleItems();
                $currencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();
                $campaignId = $this->_scopeConfig->getValue('tracking_section/general/campaign_id');

                $data = [
                    'event' => 'widget_converted',
                    'visitor_id' => $this->_cookieManager->getCookie('ps_analytics'),
                    'order_id' => $order->getIncrementId(),
                    'referrer' => $this->_storeManager->getStore()->getBaseUrl()
                ];
                if ($campaignId) {
                    $data['album_id'] = $campaignId;
                }

                foreach ($items as $item) {
                    $data['products'][$item->getProduct()->getSku()] = ['count' => $item->getQtyOrdered(), 'price' => $item->getPrice(), 'currency' => $currencyCode];
                }

                $uri = 'http://api.photoslurp.com/v1/widgets/record/';
                $client = new \Zend_Http_Client();
                $client->setUri($uri);
                $client->setRawData(json_encode($data), 'application/json');
                $response = $client->request(\Zend_Http_Client::POST);
            }
        }
    }
}
