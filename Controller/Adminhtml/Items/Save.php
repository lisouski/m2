<?php
/**
 * Copyright © 2015 Photoslurp. All rights reserved.
 */

namespace Photoslurp\Pswidget\Controller\Adminhtml\Items;

use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Photoslurp\Pswidget\Controller\Adminhtml\Items
{
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            try {
                $model = $this->_objectManager->create('Photoslurp\Pswidget\Model\Items');
                $data = $this->getRequest()->getPostValue();
                $inputFilter = new \Zend_Filter_Input(
                    [],
                    [],
                    $data
                );
                $data = $inputFilter->getUnescaped();
                $id = $this->getRequest()->getParam('id');
                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        throw new \Magento\Framework\Exception\LocalizedException(__('The wrong item is specified.'));
                    }
                    $params = array_map(
                        function () {
                            return null;
                        },
                        $model->getData()
                    );
                    $data = array_merge($params, $data);
                }
                $model->setData($data);
                $session = $this->_objectManager->get('Magento\Backend\Model\Session');
                $session->setPageData($model->getData());
                $model->save();

                if (isset($data['css'])) {
                    $filesystem = $this->_objectManager->get('Magento\Framework\Filesystem');
                    $writer = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);

                    $file = $writer->openFile('photoslurp/'.$data['widget_id'] . '.css', 'w');
                    try {
                        $file->lock();
                        try {
                            $file->write($data['css']);
                        }
                        finally {
                            $file->unlock();
                        }
                    }
                    finally {
                        $file->close();
                    }
                }
                
                $storeManager = $this->_objectManager->create('Magento\Store\Model\StoreManagerInterface');

                $stores = $storeManager->getStores($withDefault = false);


                foreach ($stores as $store) {
                    $storeId = $store->getId();

                    $modelLang = $this->_objectManager->create('Photoslurp\Pswidget\Model\ItemsLang');

                    $itemLangId = $modelLang->getCollection()
                        ->addFilter('id_widget', $id)
                        ->addFilter('id_lang', $storeId)
                        ->getFirstItem()
                        ->getId();

                    $modelLang->setData(
                        [
                            'id' => $itemLangId,
                            'id_widget' => $model->getId(),
                            'id_lang' => $storeId,
                            'shop_this_look_text' => $model->getData('shop_this_look_text_' . $storeId),
                            'submit_text' => $model->getData('submit_text_' . $storeId),
                            'cta_button' => $model->getData('cta_button_' . $storeId),
                            'load_more_text' => $model->getData('load_more_text_' . $storeId),
                            'posted_by_text' => $model->getData('posted_by_text_' . $storeId),
                            'view_and_shop_text' => $model->getData('view_and_shop_text_' . $storeId),
                            'add_photos_img' => $model->getData('add_photos_img_' . $storeId),
                        ]
                    )->save();
                }


                $this->messageManager->addSuccess(__('You saved the item.'));
                $session->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('photoslurp_pswidget/*/edit', ['id' => $model->getId()]);
                    return;
                }
                $this->_redirect('photoslurp_pswidget/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('id');
                if (!empty($id)) {
                    $this->_redirect('photoslurp_pswidget/*/edit', ['id' => $id]);
                } else {
                    $this->_redirect('photoslurp_pswidget/*/new');
                }
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong while saving the item data. Please review the error log.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($data);
                $this->_redirect('photoslurp_pswidget/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->_redirect('photoslurp_pswidget/*/');
    }
}
