<?php
/**
 * Copyright © 2015 Photoslurp. All rights reserved.
 */

namespace Photoslurp\Pswidget\Controller\Adminhtml\Items;

class Edit extends \Photoslurp\Pswidget\Controller\Adminhtml\Items
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Photoslurp\Pswidget\Model\Items');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This item no longer exists.'));
                $this->_redirect('photoslurp_pswidget/*');
                return;
            }
        }
        // set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->_coreRegistry->register('current_photoslurp_pswidget_items', $model);

        $translations = [];
        $storeManager = $this->_objectManager->create('Magento\Store\Model\StoreManagerInterface');

        if ($id) {
            foreach ($storeManager->getStores() as $store) {
                $modelLang = $this->_objectManager->create('Photoslurp\Pswidget\Model\ItemsLang');
                $idLangItem = $modelLang->getCollection()
                    -> addFilter('id_widget', $id)
                    -> addFilter('id_lang', $store->getId())
                    -> getFirstItem()
                    -> getId();
                $translations[] = $modelLang->load($idLangItem);
            }
        }

        $this->_coreRegistry->register('current_photoslurp_pswidget_items_lang', $translations);

        $this->_initAction();
        $this->_view->getLayout()->getBlock('items_items_edit');
        $this->_view->renderLayout();
    }
}
