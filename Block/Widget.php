<?php

namespace Photoslurp\Pswidget\Block;

class Widget extends \Magento\Framework\View\Element\Template
{
    private $_widget = null;

    private $_params = null;

    private $_containerId = null;

    private $_userName = null;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Photoslurp\Pswidget\Model\Items $widgets,
        \Photoslurp\Pswidget\Model\ItemsLang $widgetsLang,
        \Magento\Framework\Locale\Resolver $resolver,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
    
        parent::__construct($context);

        if (!isset($data['widget_id'])) {
            return;
        }

        $this->_widget = $widgets->load($data['widget_id']);

        if (!$this->_widget->getWidgetEnable()) {
            return;
        }

        $langParams = $widgetsLang->getCollection()
            ->addFilter('id_widget', $data['widget_id'])
            ->addFilter('id_lang', $this->_storeManager->getStore()->getId())
            ->getFirstItem()->getData();

        $params = array_merge($this->_widget->getData(), $langParams);
        $params['lang'] = $resolver->getLocale().'_'. $this->_storeManager->getStore()->getId();

        if ($product = $registry->registry('current_product')) {
            $params['product_id'] = $this->prepareProductIdParam($product);
        }

        if ($additionalParams = json_decode('{'.$this->_widget->getAdditionalParams().'}', true)) {
            $params = array_merge($params, $additionalParams);
        }

        $this->_params = $params;
        $this->_containerId = 'ps_container_'.$this->_widget->getId();
        $this->_userName = $this->_widget->getUserName();

        $this->setTemplate('Photoslurp_Pswidget::widget.phtml');
    }

    private function prepareProductIdParam($product)
    {
        if ($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            $children = $product->getTypeInstance()->getUsedProducts($product);
            $skus[] = $product->getSku();
            foreach ($children as $child) {
                $skus[] = $child->getSku();
            }
            $productIdParam = $skus;
        } else {
            $productIdParam = $product->getSku();
        }
        return $productIdParam;
    }

    public function getParams()
    {
        return $this->_params;
    }

    public function getContainerId()
    {
        return $this->_containerId;
    }

    public function getUserName()
    {
        return $this->_userName;
    }

    public function getWidget()
    {
        return $this->_widget;
    }

    public function getJson()
    {
        $data = $this->_params;

        if ($data['utm_params'] == 2) {//custom parameters
            $data['utm_params'] = [];
            if (isset($data['utm_source'])) {
                $data['utm_params']['utm_source'] = $data['utm_source'];
            }

            if (isset($data['utm_medium'])) {
                $data['utm_params']['utm_medium'] = $data['utm_medium'];
            }

            if (isset($data['utm_campaign'])) {
                $data['utm_params']['utm_campaign'] = $data['utm_campaign'];
            }

            if (isset($data['utm_content'])) {
                $data['utm_params']['utm_content'] = $data['utm_content'];
            }
        } elseif ($data['utm_params']!== null) {
            $data['utm_params'] = (bool)$data['utm_params'];
        }

        if (isset($data['collection'])) {
            $data['collection'] = array_map('trim', explode(",", $data['collection']));
        }

        $boolAttributes = [
            'enable_g_a',
            'allow_empty',
            'show_submit',
            'social_icons',
            'random_order',
            'in_stock_only',
            'rights_cleared_only',
            'assigned_only',
            'thumb_overlay',
            'varying_thumb_sizes',
            'auto_scroll_carousel',
            'strict_products',
            'same_tab_links'
        ];

        foreach ($boolAttributes as $name) {
            if ($data[$name] !== null) {
                $data[$name] = (bool)$data[$name];
            }
        }

        $intAttributes = [
            'album_id',
            'autoscroll_limit',
            'page_limit',
            'init_delay',
            'analytics_cookie_TTL',
            'empty_threshold',
            'visible_products'
        ];

        foreach ($intAttributes as $name) {
            if ($data[$name] !== null) {
                $data[$name] = (int)$data[$name];
            }
        }

        unset($data['id']);
        unset($data['widget_enable']);
        unset($data['position']);
        unset($data['additional_params']);
        unset($data['user_name']);
        unset($data['id_widget']);
        unset($data['id_lang']);
        unset($data['css']);

        unset($data['utm_source']);
        unset($data['utm_medium']);
        unset($data['utm_campaign']);
        unset($data['utm_content']);
        unset($data['page_type']);
        unset($data['website']);
        $parameters = [];
        foreach ($data as $path => $value) {
            if (($value !== null)) {
                if (strpos($path, 'style_')=== 0) {
                    $temp = &$parameters;
                    foreach (explode('_', $path) as $key) {
                        $temp = &$temp[$key];
                    }

                    $temp = $value;
                    unset($temp);
                } else {
                    $jsName = preg_replace_callback(
                        '/_([^_])/',
                        function (array $m) {
                            return ucfirst($m[1]);
                        },
                        $path
                    );
                    $parameters[$jsName] = $value;
                }
            }
        }

        return json_encode($parameters);
    }

    public function getCssUrl()
    {
        return $this ->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'photoslurp/'.$this->_widget->getWidgetId().'.css';
    }
}
