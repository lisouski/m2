<?php
/**
 * Copyright © 2015 Photoslurp. All rights reserved.
 */

// @codingStandardsIgnoreFile

namespace Photoslurp\Pswidget\Block\Adminhtml\Items\Edit\Tab;


use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;



class Main extends Generic implements TabInterface
{
    private $_stores = null;

    private $_storesCount = null;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    )
    {
        parent::__construct($context,$registry,$formFactory);
        $this->_stores = $this->_storeManager->getStores($withDefault = false);
        $this->_storesCount = count($this->_stores);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('General');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_photoslurp_pswidget_items');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('item_');
        $form->getFieldsetElementRenderer()->setTemplate('Photoslurp_Pswidget::widget/form/renderer/fieldset/element.phtml');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General')]);
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'widget_enable',
            'select',
            ['name' => 'widget_enable', 'label' => __('Enable'), 'title' => __('Enable'), 'required' => false,
                'options' => ['0'=>'No','1' => 'Yes'],
            ]
        );
        $fieldset->addField(
            'user_name',
            'text',
            ['name' => 'user_name', 'label' => __('User Name'), 'title' => __('User Name'), 'required' => true,
                'note' => __(
                    'Your Photoslurp user name.'
                ),
            ]
        );
        $fieldset->addField(
            'widget_id',
            'text',
            ['name' => 'widget_id', 'label' => __('Widget Id'), 'title' => __('Widget Id'), 'required' => true,
                'note' => __(
                    'A unique identifier for each of the widgets you use on your website.'
                ),
            ]
        );
        $fieldset->addField(
            'widget_type',
            'select',
            ['name' => 'widget_type', 'label' => __('Widget Type'), 'title' => __('Widget Type'), 'required' => true,
                'options' => ['carousel'=>'Carousel','gallery' => 'Gallery',
                    'circular'=>'Circular','masonry'=>'Masonry'],
                'note' => __(
                    'This is either set to carousel or gallery depending on how you would like to display your pictures. Carousel has been specifically designed to be placed on your product pages, and Gallery to be used as a separate gallery page.'
                ),
            ]

        );
        $fieldset->addField(
            'album_id',
            'text',
            ['name' => 'album_id', 'label' => __('Album Id'), 'title' => __('Album Id'), 'required' => true,
                'note' => __(
                    'This is the ID number of your campaign.'
                ),
            ]
        );
        $fieldset->addField(
            'page_limit',
            'text',
            ['name' => 'page_limit', 'label' => __('Page Limit'), 'title' => __('Page Limit'), 'required' => true, 'value'=>'15',
                'note' => __(
                    'The number of images to load per page for both the Gallery and Carousel modes.'
                ),
            ]
        );
        $fieldset->addField(
            'page_type',
            'select',
            ['name' => 'page_type', 'label' => __('Page Type'), 'title' => __('Page Type'), 'required' => true,
                'options' => ['home'=>'Home','product' => 'Product','lookbook' => 'Lookbook'],
                'note' => __(
                    'This parameter is used in analytics calculations.'
                ),
            ]
        );

        foreach ($this->_storeManager->getWebsites() as $site){
            $websites[$site->getId()] = $site->getName();
        }

        $fieldset->addField(
            'website',
            'select',
            [
                'name' => 'website',
                'label' => __('Website'),
                'values' => $websites,
            ]
        );

        $fieldset->addField(
            'theme',
            'select',
            ['name' => 'theme', 'label' => __('Theme'), 'title' => __('Theme'),
                'options' => ['retro'=>'Retro','modern' => 'Modern','dark' => 'Dark'],
                'note' => __(
                    'Set the display theme used for the widget.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'position',
            'select',
            ['name' => 'position', 'label' => __('Position'), 'title' => __('Position'),
                'options' => [
                    'content'=>'Product Content Bottom',
                    'product.info.media'=>'After Product Info Media',
                    'product.info.details'=>'After Product Info Details'
                    ],
                'note' => __(
                    'Position on product page.'
                ),
            ]
        );

        $fieldset->addField(
         'show_submit',
         'select',
         ['name' => 'show_submit', 'label' => __('Show Submit'), 'title' => __('Show Submit'), 'required' => false,
             'options' => ['0'=>'No','1' => 'Yes'],
             'note' => __(
                 'This parameter will enable the frontend upload widget, and display a Submit button.'
             ),
             'use_default'=>true
         ]
        );

        $i = 0;
        $isLast = false;
        foreach($this->_stores as $store){
            if($i == $this->_storesCount - 1) $isLast = true;
            $fieldset->addField(
                'submit_text_'.$store->getId(),
                'text',
                ['name' => 'submit_text_'.$store->getId(), 'label' => __(($i==0)?'Submit Text':''), 'title' => __('Submit Text'),
                    'note' => $store->getCode() . ($isLast?'<br><br>This is the text that will be displayed on the Submit button.':'')
                ]
            );$i++;
        };
        $fieldset->addField(
            'toc_link',
            'text',
            ['name' => 'toc_link', 'label' => __('T&C Link for Frontend Uploader'), 'title' => __('T&C Link for Frontend Uploader'), 'required' => true,
                'note' => __(
                    'The link to your T&C page that users will have to agree to when uploading photos using the frontend uploader.'
                ),
            ]
        );

        $fieldset->addField(
            'social_icons',
            'select',
            ['name' => 'social_icons', 'label' => __('Social Icons'), 'title' => __('Social Icons'), 'required' => false,
                'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Enable social icons in the lightbox to allow users to share pictures back to social networks.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'random_order',
            'select',
            ['name' => 'random_order', 'label' => __('Photo Order'), 'title' => __('Photo Order'), 'required' => false,
                'options' => ['0'=>'Newest First','1' => 'Random'],
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'allow_empty',
            'select',
            ['name' => 'allow_empty', 'label' => __('Allow Empty'), 'title' => __('Allow Empty'), 'required' => false,
                'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'When set to no, this option will ensure that all photos are returned on product pages when a product does not have any photos associated with it.'
                ),
                'use_default'=>true

            ]
        );

        $i = 0;
        $isLast = false;
        foreach($this->_stores as $store){
            if($i == $this->_storesCount - 1) $isLast = true;
            $fieldset->addField(
                'add_photos_img_'.$store->getId(),
                'text',
                ['name' => 'add_photos_img_'.$store->getId(), 'label' => __(($i==0)?'Add Photos Img':''), 'title' => __('Add Photos Img'),
                    'note' => $store->getCode(). ($isLast?'<br><br>This requires a URL to an image that will be displayed when there are no images yet in a particular album (for example when the productId parameter is used, and there are no photos for particular product yet). A banner inviting users to submit images is recommended to be used here.':''),
                    'use_default'=>true
                ]
            );$i++;
        };

        $i = 0;
        foreach($this->_stores as $store){
            $fieldset->addField(
                'shop_this_look_text_'.$store->getId(),
                'text',
                ['name' => 'shop_this_look_text_'.$store->getId(), 'label' => __(($i==0)?'Shop This Look Text':''), 'title' => __('Shop This Look Text'),
                    'note' => $store->getCode(),
                    'use_default'=>true
                ]
            );$i++;
        };

        // define field dependencies
        $blockDependence = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );

        $blockDependence->addFieldMap(
            "item_allow_empty",
            'allow_empty'
        );

        $blockDependence->addFieldMap("item_show_submit", 'show_submit');
        foreach($this->_stores as $store) {
            $id = $store->getId();
            $blockDependence->addFieldMap(
                "item_submit_text_".$id,
                'submit_text_'.$id
            )->addFieldDependence(
                'submit_text_'.$id,
                'show_submit',
                '1'
            );

            $blockDependence->addFieldMap(
                "item_add_photos_img_".$id,
                'add_photos_img_'.$id
            )->addFieldDependence(
                'add_photos_img_'.$id,
                'allow_empty',
                '1'
            );
        }

        $blockDependence
            ->addFieldMap("item_toc_link", 'toc_link')
            ->addFieldDependence(
                'toc_link',
                'show_submit',
                '1'
            );

        $blockDependence
            ->addFieldMap("item_page_type", 'page_type')
            ->addFieldMap("item_position", 'position')
            ->addFieldDependence(
                'position',
                'page_type',
                'product'
            );

        $this->setChild(
            'form_after',
            $blockDependence
        );


        $_data = $model->getData();

        $translations = $this->_coreRegistry->registry('current_photoslurp_pswidget_items_lang');

        if($translations){
            foreach ($translations as $translation){
                if($translation->getId()){
                    $widget_lang_data = $translation->getData();
                    $form_data['submit_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['submit_text'];
                    $form_data['shop_this_look_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['shop_this_look_text'];
                    $form_data['add_photos_img_'.$widget_lang_data['id_lang']] = $widget_lang_data['add_photos_img'];
                    $_data = array_merge($_data, $form_data);
                }
            }
        }

        if (!empty($_data)) {
            $form->setValues($_data);
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
