<?php
/**
 * Copyright © 2015 Photoslurp. All rights reserved.
 */

// @codingStandardsIgnoreFile

namespace Photoslurp\Pswidget\Block\Adminhtml\Items\Edit\Tab;


use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;



class Advanced extends Generic implements TabInterface
{
    private $_stores = null;
    private $_storesCount = null;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    )
    {
        parent::__construct($context,$registry,$formFactory);
        $this->_stores = $this->_storeManager->getStores($withDefault = false);
        $this->_storesCount = count($this->_stores);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Advanced');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Advanced');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_photoslurp_pswidget_items');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('item_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Advanced Parameters')]);
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'autoscroll_limit',
            'text',
            ['name' => 'autoscroll_limit', 'label' => __('Autoscroll Limit'), 'title' => __('Autoscroll Limit'),
                'note' => __(
                    'The number of pages to load before showing a Load More button. Leave blank for infinite scroll.'
                ),
                'use_default'=>true
            ]
        );

        $i = 0;
        $isLast = false;
        foreach($this->_stores as $store){
            if($i == $this->_storesCount - 1) $isLast = true;
            $fieldset->addField(
                'load_more_text_'.$store->getId(),
                'text',
                ['name' => 'load_more_text_'.$store->getId(), 'label' => __(($i==0)?'Load More Text':''), 'title' => __('Load More Text'),
                    'note' => $store->getCode() . ($isLast?'<br><br>Related to the autoscrollLimit parameter, this parameter relates to the text that will be displayed on the Load More button.':''),
                    'use_default'=>true
                ]
            );$i++;
        };

        $fieldset->addField(
            'enable_g_a',
            'select',
            ['name' => 'enable_g_a', 'label' => __('Enable GA'), 'title' => __('Enable GA'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Enable Google Analytics. When this is enabled, all events collected from Photoslurp widgets will also be sent to the Google analytics property already configured on your website.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'init_delay',
            'text',
            ['name' => 'init_delay', 'label' => __('Init Delay'), 'title' => __('Init Delay'),
                'note' => __(
                    'In cases where other page elements end up interfering with the Photoslurp widget, a millisecond value can be set here to delay it’s loading and prevent interference.'
                ),
                'use_default'=>true
            ]
        );

        $i = 0;
        $isLast = false;
        foreach($this->_stores as $store){
            if($i == $this->_storesCount - 1) $isLast = true;
            $fieldset->addField(
                'cta_button_'.$store->getId(),
                'text',
                ['name' => 'cta_button_'.$store->getId(), 'label' => __(($i==0)?'CTA Button':''), 'title' => __('CTA Button'),
                    'note' => $store->getCode(). ($isLast?'<br><br>The text to be placed on a CTA button in the lightbox underneath the product image. Blank disables button.':''),
                    'use_default'=>true
                ]
            );$i++;
        };

        $fieldset->addField(
            'thumb_overlay',
            'select',
            ['name' => 'thumb_overlay', 'label' => __('Thumb Overlay'), 'title' => __('Thumb Overlay'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Enable mouseover effect on thumbnails in Gallery mode showing author details.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'varying_thumb_sizes',
            'select',
            ['name' => 'varying_thumb_sizes', 'label' => __('Varying Thumb Sizes'), 'title' => __('Varying Thumb Sizes'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Enable varying sized thumbnails in gallery mode.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'auto_scroll_carousel',
            'select',
            ['name' => 'auto_scroll_carousel', 'label' => __('Auto Scroll Carousel'), 'title' => __('Auto Scroll Carousel'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Enable auto scrolling of images in carousel mode.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'analytics_cookie_TTL',
            'text',
            ['name' => 'analytics_cookie_TTL', 'label' => __('Analytics Cookie TTL'), 'title' => __('Analytics Cookie TTL'),
                'note' => __(
                    'Sets the TTL (in days) of the cookie we set on user’s browsers for their interactions to count towards our conversion analytics.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'submission_form_CSS_URL',
            'text',
            ['name' => 'submission_form_CSS_URL', 'label' => __('Use a custom CSS file for the frontend uploader'), 'title' => __('Use a custom CSS file for the frontend uploader'),
                'note' => __(
                    'Enter a custom CSS URL here to change the look and feel of the frontend uploader.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'strict_products',
            'select',
            ['name' => 'strict_products', 'label' => __('Strict Products'), 'title' => __('Strict Products'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Ensures that product related photos are shown first, followed by all other photos in the campaign. Note: This parameter will overwrite allowEmpty.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'empty_threshold',
            'text',
            ['name' => 'empty_threshold', 'label' => __('Empty Threshold'), 'title' => __('Empty Threshold'), 'required' => false,
                'note' => __(
                    'Set a threshold of the minimum number of product related images that should exist before showing our carousel.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'in_stock_only',
            'select',
            ['name' => 'in_stock_only', 'label' => __('In Stock Only'), 'title' => __('In Stock Only'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Only returns photos to products that are in stock. Feature needs to be enabled for your account.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'rights_cleared_only',
            'select',
            ['name' => 'rights_cleared_only', 'label' => __('Rights Cleared Only'), 'title' => __('Rights Cleared Only'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Only return photos for which media rights have been granted.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'assigned_only',
            'select',
            ['name' => 'assigned_only', 'label' => __('Assigned Only'), 'title' => __('Assigned Only'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Only return photos that have products associated with them.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'visible_products',
            'text',
            ['name' => 'visible_products', 'label' => __('Visible Products'), 'title' => __('Visible Products'),
                'note' => __(
                    'Accepts a number between 2-6.'
                ),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'collection',
            'text',
            ['name' => 'collection', 'label' => __('Collections'), 'title' => __('Collections'),
                'note' => __(
                    'Enter the name of one or more collections to filter results by.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'cookie_domain',
            'text',
            ['name' => 'cookie_domain', 'label' => __('Cookie Domain'), 'title' => __('Cookie Domain'),
                'note' => __(
                    'If specified all widget cookies will be set on given domain.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'utm_params',
            'select',
            ['name' => 'utm_params', 'label' => __('Utm Params'), 'title' => __('Utm Params'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes','2'=>'Custom'],
                'note' => __(
                    'When set to true, all product URL’s will be appended with
                                Google Analytics UTM parameters
                                (&utm_source=photoslurp&utm_medium=widget&utm_campaign=albumid&utm_co
                                ntent=productId). This parameter also accepts an object with custom values for each
                                UTM parameter.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'utm_source',
            'text',
            ['name' => 'utm_source', 'label' => __('Utm Source'), 'title' => __('Utm Source'),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'utm_medium',
            'text',
            ['name' => 'utm_medium', 'label' => __('Utm Medium'), 'title' => __('Utm Medium'),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'utm_campaign',
            'text',
            ['name' => 'utm_campaign', 'label' => __('Utm Campaign'), 'title' => __('Utm Campaign'),
                'use_default'=>true
            ]
        );
        $fieldset->addField(
            'utm_content',
            'text',
            ['name' => 'utm_content', 'label' => __('Utm Content'), 'title' => __('Utm Content'),
                'use_default'=>true
            ]
        );

        $i = 0;
        $isLast = false;
        foreach($this->_stores as $store){
            if($i == $this->_storesCount - 1) $isLast = true;
            $fieldset->addField(
                'posted_by_text_'.$store->getId(),
                'text',
                ['name' => 'posted_by_text_'.$store->getId(), 'label' => __(($i==0)?'Posted By Text':''), 'title' => __('Posted By Text'),
                    'note' => $store->getCode(). ($isLast?'<br><br>Custom “Posted by” text shown when thumbOverlay is active.':''),
                    'use_default'=>true
                ]
            );$i++;
        };

        $i = 0;
        $isLast = false;
        foreach($this->_stores as $store){
            if($i == $this->_storesCount - 1) $isLast = true;
            $fieldset->addField(
                'view_and_shop_text_'.$store->getId(),
                'text',
                ['name' => 'view_and_shop_text_'.$store->getId(), 'label' => __(($i==0)?'View And Shop Text':''), 'title' => __('View And Shop Text'),
                    'note' => $store->getCode(). ($isLast?'<br><br>Custom "View and Shop" text when thumbOverlay is active.':''),
                    'use_default'=>true
                ]
            );$i++;
        };

        $fieldset->addField(
            'same_tab_links',
            'select',
            ['name' => 'same_tab_links', 'label' => __('Same Tab Links'),
                'title' => __('Same Tab Links'), 'required' => false, 'options' => ['0'=>'No','1' => 'Yes'],
                'note' => __(
                    'Opens product links in same tab or in a new tab. Default is false.'
                ),
                'use_default'=>true
            ]
        );

        $fieldset->addField(
            'additional_params',
            'textarea',
            ['name' => 'additional_params', 'label' => __('Additional Params'), 'title' => __('Additional Params')]
        );

        // define field dependencies
        $blockDependence = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );
        $blockDependence->addFieldMap(
            "item_utm_params",
            'utm_params'
        )->addFieldMap(
            "item_utm_source",
            'utm_source'
        )->addFieldDependence(
            'utm_source',
            'utm_params',
            '2'
        );

        $blockDependence->addFieldMap(
            "item_utm_params",
            'utm_params'
        )->addFieldMap(
            "item_utm_medium",
            'utm_medium'
        )->addFieldDependence(
            'utm_medium',
            'utm_params',
            '2'
        );

        $blockDependence->addFieldMap(
            "item_utm_params",
            'utm_params'
        )->addFieldMap(
            "item_utm_campaign",
            'utm_campaign'
        )->addFieldDependence(
            'utm_campaign',
            'utm_params',
            '2'
        );

        $blockDependence->addFieldMap(
            "item_utm_params",
            'utm_params'
        )->addFieldMap(
            "item_utm_content",
            'utm_content'
        )->addFieldDependence(
            'utm_content',
            'utm_params',
            '2'
        );

        $blockDependence->addFieldMap(
            "item_page_type",
            'page_type'
        )->addFieldMap(
            "item_website",
            'website'
        )->addFieldDependence(
            'website',
            'page_type',
            'product'
        );

        $this->setChild(
            'form_after',
            $blockDependence
        );

        $_data = $model->getData();

        $translations = $this->_coreRegistry->registry('current_photoslurp_pswidget_items_lang');

        if($translations){
            foreach ($translations as $translation){
                if($translation->getId()){
                    $widget_lang_data = $translation->getData();
//                    $form_data['submit_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['submit_text'];
//                    $form_data['shop_this_look_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['shop_this_look_text'];
//                    $form_data['note_add_pics_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['note_add_pics_text'];
                    $form_data['cta_button_'.$widget_lang_data['id_lang']] = $widget_lang_data['cta_button'];
                    $form_data['load_more_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['load_more_text'];
                    $form_data['posted_by_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['posted_by_text'];
                    $form_data['view_and_shop_text_'.$widget_lang_data['id_lang']] = $widget_lang_data['view_and_shop_text'];
                    $_data = array_merge($_data, $form_data);
                }
            }
        }

        if (!empty($_data)) {
            $form->setValues($_data);
        }
        $this->setForm($form);
        return parent::_prepareForm();

    }
}
