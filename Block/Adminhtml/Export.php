<?php
/**
 * Copyright © 2015 Photoslurp. All rights reserved.
 */
namespace Photoslurp\Pswidget\Block\Adminhtml;

class Export extends \Magento\Backend\Block\Template
{
    private $_directoryList = null;

    private $_defaultFileName = 'photoslurp_export.csv';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    ) {
        parent::__construct($context);
        $this->_directoryList = $directoryList;
    }

    public function getDownloadLink()
    {
        $downloadLink = '';
        $configExportPath = $this->_scopeConfig->getValue('export_section/general/path');

        if ($configExportPath) {
            if (file_exists($this->_directoryList->getRoot().'/'.$configExportPath)) {
                $downloadLink = $this->_storeManager->getStore()->getBaseUrl().$configExportPath;
            }
        } else {
            if (file_exists($this->_directoryList->getRoot().'/'.$this->_defaultFileName)) {
                $downloadLink = $this->_storeManager->getStore()->getBaseUrl() . $this->_defaultFileName;
            }
        }

        return $downloadLink;
    }

    public function getConfigUrl()
    {
        return $this->_urlBuilder->getUrl('adminhtml/system_config/edit', ['section' => 'export_section']);
    }
}
