<?php
/**
 * Copyright © 2015 Photoslurp. All rights reserved.
 */

namespace Photoslurp\Pswidget\Model;

class Export extends \Magento\Framework\Model\AbstractModel
{
    const XML_PATH_PRODUCT_URL_SUFFIX = 'catalog/seo/product_url_suffix';

    private $_delimiter = '|';

    private $_defaultFileName = 'photoslurp_export.csv';

    private $_currencyModel = null;

    private $_storeManager  = null;

    private $_scopeConfig = null;

    private $_iterator = null;

    private $_productCollection = null;

    private $_directory = null;

    private $_stores = null;

    private $_currecies = null;

    private $_baseCurrency = null;

    private $_urlMedia = null;

    private $_exportMode = null;

    private $_configExportPath = null;

    private $_directoryList = null;

    private $_fileNameToWrite = null;

    private $_productUrlSuffix = [];

    private $_objectManager = null;

    private $_categories = [];


    public function __construct(
        \Magento\Directory\Model\Currency $currencyModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        \Magento\Catalog\Model\ResourceModel\Category\Collection $categoryCollection,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Directory\Helper\Data $directory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    ) {
        $this->_currencyModel = $currencyModel;
        $this->_storeManager  = $storeManager;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_scopeConfig = $scopeConfig;
        $this->_productCollection  = $productCollection;
        $this->_iterator  = $iterator;
        $this->_directory  = $directory;

        $stores = $this->_storeManager->getStores($withDefault = false);
        if ($configStoresAllowed = $this->_scopeConfig->getValue('export_section/general/stores')) {
            $storesAllowed = explode(',', $configStoresAllowed);
            foreach ($stores as $store) {
                if (in_array($store->getId(), $storesAllowed)) {
                    $this->_stores[] = $store;
                }
            }
        } else {
            $this->_stores = $stores;
        }


        $this->_currecies = $this->_currencyModel->getConfigAllowCurrencies();
        $this->_baseCurrency = $this->_storeManager->getStore()->getBaseCurrencyCode();
        $this->_urlMedia = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $this->_exportMode = $this->_scopeConfig->getValue('export_section/general/mode');
        $this->_configExportPath = $this->_scopeConfig->getValue('export_section/general/path');
        $this->_directoryList = $directoryList;

        if ($this->_configExportPath) {
            $this->_fileNameToWrite = $this->_configExportPath;
            $dirname = dirname($this->_fileNameToWrite);
            if (!is_dir($dirname)) {
                mkdir($dirname, 0755, true);
            }
        } else {
            $this->_fileNameToWrite = $this->_directoryList->getRoot().'/'.$this->_defaultFileName;
        }

        $categoryCollection->addNameToResult();
        foreach ($categoryCollection as $category) {
            $this->_categories[$category->getId()] = $category->getName();
        }
    }

    public function exportCron()
    {
        if ($this->_scopeConfig->getValue('export_section/general/cron')) {
            $this->export();
        }
    }

    public function export()
    {

        $this->prepareCollection();

        $headerCsv = ['sku'];
        foreach ($this->_currecies as $currecy) {
            $headerCsv[] = 'price_'.$currecy;
        }
        $headerCsv[]='in_stock';
        $headerCsv[]='image_url';

        foreach ($this->_stores as $store) {
            $storeId = $store->getId();
            $localeCode = $this->_scopeConfig->getValue('general/locale/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
            $suffix = sprintf('_%s_%s', $localeCode, $storeId);
            $headerCsv[] = 'title'.$suffix;
            $headerCsv[] = 'description'.$suffix;
            $headerCsv[] = 'url'.$suffix;
        }

        $headerCsv[]='google_category';
        $headerCsv[]='gender';
        $headerCsv[]='product_types';

        $fp = fopen($this->_fileNameToWrite, 'w');
        fwrite($fp, '# exported at ' . date('d-m-Y') . PHP_EOL);
        fwrite($fp, implode($headerCsv, $this->_delimiter) . PHP_EOL);

        $this->_iterator->walk(
            $this->_productCollection->getSelect(),
            [[$this, 'walkCallback']],
            ['fp' => $fp,]
        );

        fclose($fp);
    }

    public function walkCallback($data)
    {
        $fp         = $data['fp'];
        $row        = $data['row'];
        $insertData = [$row['sku']];

        foreach ($this->_currecies as $currency) {
            $insertData[] = $this->_directory->currencyConvert($row['price'], $this->_baseCurrency, $currency);
        }

        $insertData[] = $row['is_in_stock'];

        $insertData[] = $row['image']?$this->_urlMedia .'catalog/product'.$row['image'] : '';

        $product = $this->_objectManager->get('Magento\Catalog\Model\Product');
        $product->setData($row);
        $productStores = $product->getStoreIds();

        $pattern = '/"/';
        $replacement = '$0"';

        foreach ($this->_stores as $store) {
            $id = $store->getId();
            $isProductInStore = in_array($id, $productStores);
            $insertData[] = $isProductInStore ? sprintf('"%s"', preg_replace($pattern, $replacement, $row['name_'.$id])):'';
            $insertData[] = $isProductInStore ? sprintf('"%s"', preg_replace($pattern, $replacement, $row['description_'.$id])):'';
            $insertData[] = $isProductInStore ? $store->getBaseUrl().$row['url_key_'.$id].$this->getProductUrlSuffix($id).'?___store='.$store->getCode():'';
        }

        $categoryNames = [];
        $categoryIds = $product->getCategoryIds();
        foreach ($categoryIds as $id) {
            $categoryNames[] = $this->_categories[$id];
        }

        $insertData['google_category'] = '';
        $insertData['gender'] = $product->getGender();
        $insertData['product_types'] = implode(',', $categoryNames);

        fwrite($fp, implode($insertData, $this->_delimiter) . PHP_EOL);
    }


    private function prepareCollection()
    {

        $this->_productCollection
            ->addAttributeToFilter('visibility', ["neq"=>1])
            ->addAttributeToSelect(['price','image'], 'left');

        $this->_productCollection->addWebsiteFilter($this->getWebsitesFilter());

        $this->_productCollection->joinField(
            'is_in_stock',
            'cataloginventory_stock_item',
            'is_in_stock',
            'product_id=entity_id',
            null,
            'left'
        );

        foreach ($this->_stores as $store) {
            $storeId = $store->getId();
            $this->_productCollection->joinAttribute(
                'name_'.$storeId,
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $storeId
            );
            $this->_productCollection->joinAttribute(
                'description_'.$storeId,
                'catalog_product/description',
                'entity_id',
                null,
                'left',
                $storeId
            );
            $this->_productCollection->joinAttribute(
                'url_key_'.$storeId,
                'catalog_product/url_key',
                'entity_id',
                null,
                'left',
                $storeId
            );
        }
    }

    private function getWebsitesFilter()
    {
        $websites = [];
        foreach ($this->_stores as $store) {
            $websites[] = $store->getWebsiteId();
        }
        $websites = array_unique($websites);
        return $websites;
    }

    private function getProductUrlSuffix($storeId = null)
    {
        if (!isset($this->_productUrlSuffix[$storeId])) {
            $this->_productUrlSuffix[$storeId] = $this->_scopeConfig->getValue(
                self::XML_PATH_PRODUCT_URL_SUFFIX,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }
        return $this->_productUrlSuffix[$storeId];
    }
}
